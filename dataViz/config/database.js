module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', '64.226.81.137'),
        port: env.int('DATABASE_PORT', 3306),
        database: env('DATABASE_NAME', 'dataviz'),
        username: env('DATABASE_USERNAME', 'dataViz'),
        password: env('DATABASE_PASSWORD', 'dataViz'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
